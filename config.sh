#!/bin/bash
DIR_HOME=/home/konrad
DIR_BIN=$DIR_HOME/bin
DIR_VIM=$DIR_HOME/.vim/tmp
mkdir -p $DIR_HOME/.config/i3status
[[ -d $DIR_BIN ]] || mkdir $DIR_BIN
[[ -d $DIR_VIM ]] || mkdir -p $DIR_VIM

read -rd '' FILES <<-EOF
  config/bashrc $DIR_HOME/.bashrc
  config/gitconfig $DIR_HOME/.gitconfig
  config/profile $DIR_HOME/.profile
  config/vimrc $DIR_HOME/.vimrc
  config/Xmodmap $DIR_HOME/.Xmodmap
  config/Xresources $DIR_HOME/.Xresources
  config/i3.conf $DIR_HOME/.config/i3/config
  config/i3status.conf $DIR_HOME/.config/i3status/config
  config/gtk.css $DIR_HOME/.config/gtk-3.0/gtk.css
EOF

read -rd '' FILES_BIN <<-EOF
  bin/brightness.sh $DIR_BIN/brightness.sh
  bin/clean_docker.sh $DIR_BIN/clean_docker.sh
  bin/deduplicate.sh $DIR_BIN/deduplicate.sh
  bin/disk $DIR_BIN/disk
  bin/eh_clean.sh $DIR_BIN/eh_clean.sh
  bin/git-completion.bash $DIR_BIN/git-completion.bash
  bin/ipod.sh $DIR_BIN/ipod.sh
  bin/recordScreen.sh $DIR_BIN/recordScreen.sh
  bin/start_docker.sh $DIR_BIN/start_docker.sh
  bin/start_jackd.sh $DIR_BIN/start_jackd.sh
EOF

function update_file() {
  local SRC DEST
  SRC=$1
  DEST=$2
  if [[ ! -f $DEST ]]; then
    cp $SRC $DEST
  fi
  diff --color=always -u $DEST $SRC > /dev/null
  if [[ $? == 1 ]]; then
    while true; do
      echo FILE: $DEST
      echo The file is different, options:
      echo "  a) show diff"
      echo "  b) apply changes"
      echo "  c) leave as is"
      read -p '> ' answer < /dev/tty
      if [[ "$answer" == "a" ]]; then
        diff --color=always -u $DEST $SRC
      elif [[ "$answer" == "b" ]]; then
        cp $SRC $DEST
        break
      elif [[ "$answer" == "c" ]]; then
        break
      fi
    done
  fi
}

while read SRC DEST; do
  echo ====== Copy $SRC to $DEST ========
  update_file $SRC $DEST
done <<< "$FILES"

while read SRC DEST; do
  echo ====== Copy $SRC to $DEST ========
  update_file $SRC $DEST
done <<< "$FILES_BIN"

# Other config
git config --global user.email "kpisarczyk@gmail.com"
git config --global user.name "konradp"
#timedatectl set-timezone "Europe/London"
