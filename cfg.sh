#!/bin/bash
if [[ ! -f .profile ]]; then
  # Choose a profile if not yet set
  echo No profile set. Choose profile
  PROFILES=$(ls profiles)
  while true; do
    # Loop until a profile chosen
    while read PROFILE; do
      echo - $PROFILE
    done <<<$PROFILES
    read -p '> ' answer
    echo $answer
  done
  exit 0
else
  echo Profile set
  exit 0
fi

HOME=/home/konrad
BIN=$HOME/bin
DIR_VIM=$HOME/.vim/tmp
mkdir -p $HOME/.config/i3status
[[ -d $BIN ]] || mkdir $BIN
[[ -d $DIR_VIM ]] || mkdir -p $DIR_VIM

read -rd '' FILES <<-EOF
  config.work/bash_aliases $HOME/.bash_aliases
  config.work/bashrc $HOME/.bashrc
  config.work/gitconfig $HOME/.gitconfig
  #config.work/profile $HOME/.profile
  #config.work/vimrc $HOME/.vimrc
  #config.work/Xmodmap $HOME/.Xmodmap
  config.work/Xresources $HOME/.Xresources
  config.work/i3.conf $HOME/.config/i3/config
  config.work/i3status.conf $HOME/.config/i3status/config
  #config.work/gtk.css $HOME/.config/gtk-3.0/gtk.css
EOF

read -rd '' FILES_BIN <<-EOF
  bin/brightness.sh $BIN/brightness.sh
  bin/clean_docker.sh $BIN/clean_docker.sh
  bin/deduplicate.sh $BIN/deduplicate.sh
  bin/disk $BIN/disk
  bin/eh_clean.sh $BIN/eh_clean.sh
  bin/git-completion.bash $BIN/git-completion.bash
  bin/ipod.sh $BIN/ipod.sh
  bin/recordScreen.sh $BIN/recordScreen.sh
  bin/start_docker.sh $BIN/start_docker.sh
  bin/start_jackd.sh $BIN/start_jackd.sh
EOF

function update_file() {
  # If file does not exist, create it (after prompt)
  # If file already exists, show the diff, update it, or leave as is
  local SRC DEST
  SRC=$1
  DEST=$2
  if [[ ! -f $SRC ]]; then
    echo Skip $SRC
    return
  fi
  if [[ ! -f $DEST ]]; then
    # TODO: duplicate
    while true; do
      echo FILE: $DEST
      echo The file does not exist, options:
      echo "  a) show diff"
      echo "  b) apply changes"
      echo "  c) leave as is"
      read -p '> ' answer < /dev/tty
      if [[ "$answer" == "a" ]]; then
        diff --color=always -u /dev/null $SRC
      elif [[ "$answer" == "b" ]]; then
        cp $SRC $DEST
        break
      elif [[ "$answer" == "c" ]]; then
        break
      fi
    done
  fi
  diff --color=always -u $DEST $SRC > /dev/null
  if [[ $? == 1 ]]; then
    while true; do
      echo FILE: $DEST
      echo The file is different, options:
      echo "  a) show diff"
      echo "  b) apply changes"
      echo "  c) leave as is"
      read -p '> ' answer < /dev/tty
      if [[ "$answer" == "a" ]]; then
        diff --color=always -u $DEST $SRC
      elif [[ "$answer" == "b" ]]; then
        cp $SRC $DEST
        break
      elif [[ "$answer" == "c" ]]; then
        break
      fi
    done
  fi
}

while read SRC DEST; do
  echo ====== Copy $SRC to $DEST ========
  update_file $SRC $DEST
done <<< "$FILES"

# Other
xrdb ~/.Xresources

#while read SRC DEST; do
#  echo ====== Copy $SRC to $DEST ========
#  update_file $SRC $DEST
#done <<< "$FILES_BIN"
