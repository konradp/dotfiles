#!/bin/bash
PACKAGES=(
  automake
  build-essential
  chromium
  curl
  gcc
  i3-wm i3status suckless-tools
  #lightdm
  network-manager
  nodejs
  vim-gtk
  #weechat
  #xorg
  xpdf
  xterm
)

sudo apt -y install ${PACKAGES[@]}

# Install node
#curl -sL https://deb.nodesource.com/setup_9.x | sudo bash -
#sudo apt -y install nodejs
