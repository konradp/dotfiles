#!/bin/bash
DIR="/data3"

find $DIR \
    -not -empty \
    -type f \
    -printf "%s\n" \
| sort --reverse \
    --numeric-sort \
| uniq --repeated \
| xargs -I{} -n1 find -type f -size {}c -print0 \
| xargs -0 md5sum \
| sort \
| uniq -w32 --all-repeated=separate

