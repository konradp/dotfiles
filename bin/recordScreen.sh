#!/bin/bash
# This script records screen for
# specified number of seconds (10 by default)

# This records screen
# If no recording length given, record for 5 seconds
[[ -z $1 ]] && LENGTH=10 || LENGTH="$1"
FILE="$(mktemp --suffix=.gif)"

notify-send "Record screen" \
    "Recording screen ($LENGTH seconds) to $FILE" \
    --icon=dialog-information
byzanz-record -d $LENGTH -x 0 -y 0 -w 680 -h 379 "$FILE"

