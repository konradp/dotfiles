#!/bin/bash
if [[ $(docker ps -aq) ]]; then
    echo "Cleaning"
    docker rm $(docker ps -aq)
    docker rmi $(docker images -q)
else
    echo "Nothing to clean"
fi
