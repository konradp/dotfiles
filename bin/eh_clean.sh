#!/bin/bash
EH=eh

function container:is:on() {
  # Usage: $0 UUID
  # Output:
  #   - 0 is on
  #   - 1 is off
  #   - 2 error
  [[ -z $1 ]] && return 2
  local STATUS
  read _ STATUS <<<"$($EH servers $1 info | grep status)"
  if [[ $STATUS == "active" ]]; then
    return 0
  elif [[ $STATUS == "stopped" ]]; then
    return 1
  else
    return 2
  fi
}

function container:get:gitlab() {
  # UUIDs of containers with 'gitlab:' in their name
  local KEY VALUE
  while read KEY VALUE; do
    # Gather fields
    [[ $KEY == name ]] && NAME=$VALUE
    [[ $KEY == server ]] && ID=$VALUE
    [[ $KEY == type ]] && TYPE=$VALUE
    if [[ -z $KEY ]]; then
      # Process
      if [[ $TYPE == "container" \
        && $NAME == gitlab:* ]]; then
        echo $ID
      fi
    fi
  done<<<$($EH servers info)
}

function folder:get:gitlab() {
  # UUIDs of folders with 'gitlab:' in their name
  local KEY VALUE
  while read KEY VALUE; do
    [[ $KEY == folder ]] && ID=$VALUE
    [[ $KEY == name ]] && NAME=$VALUE
    if [[ -z $KEY ]]; then
      if [[ $NAME == gitlab:* ]]; then
        echo $ID
      fi
    fi
  done<<<$($EH folders info)
}


# Power off and destroy containers
LIST=$(container:get:gitlab)
if [[ ! -z $LIST ]]; then
  echo Destroy containers
  while read ID; do
    container:is:on "$ID"
    ISON=$?
    if [[ $ISON -eq 0 ]]; then
      echo Stop $ID
      $EH -p servers $ID stop
      echo Destroy $ID
      $EH -p servers $ID destroy
    elif [[ $ISON -eq 1 ]]; then
      echo Destroy $ID
      $EH -p servers $ID destroy
    else
      echo ERROR $ID
      continue
    fi
  done<<<$LIST
else
  echo Found no containers to destroy
fi

FOLDERS=$(folder:get:gitlab)
if [[ ! -z $FOLDERS ]]; then
  echo Destroy folders
  while read ID; do
    echo Destroy folder $ID
    $EH -p folders $ID destroy
  done<<<$(folder:get:gitlab)
else
  echo Found no folders to destroy
fi
