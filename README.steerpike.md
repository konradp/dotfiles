Lenovo latop Debian:
Set up wifi as in
https://unix.stackexchange.com/questions/283722/how-to-connect-to-wifi-from-command-line
```
sudo apt install git
cd ~ && mkdir code && cd code
git clone https://gitlab.com/konradp/dotfiles.git
cd dotfiles
./install_debian.sh
./config.sh
```

# Various
background
```
feh --bg-center file.png
```
